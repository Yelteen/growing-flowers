package com.gelberkaktus.growingflowers;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSeeds;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Data {
	//Version
	public static final String VERSION = "1.8beta";
	//Proxies
	public static final String clientproxy = "com.gelberkaktus.growingflowers.ClientProxy";
	public static final String serverproxy ="com.gelberkaktus.growingflowers.CommonProxy";
	
	public static final String Required = "required-after:GFPure";//@"+VERSION;
	
	/**
	* declares easyly a seed
	* @param Plant the plant the seed spawns when using
	* @param Texture the texture of the seed like "mod:seed"
	* @param name the unlocalized name
	* @return a fully declared seed
	*/
	public static Item addSeed(Block Plant, /*String Texture,*/ String name){
		Item seed = new ItemSeeds(Plant, Blocks.farmland)
				.setUnlocalizedName(name);
	    return seed;
	}
		
	
	//rezept fur Samen aus 2x Pflanze
	/**
	 * registers a recipe for a seed, crafted from flower
	 * @param Flower the flower based for this seed
	 * @param Seed the seed, which will be crafted
	 */
	
	public static void SeedRecipe(Block Flower, Item Seed){
		GameRegistry.addRecipe(new ItemStack(Seed,2), "x", "x", 'x',new ItemStack(Flower));
	}
	/**
	 * registers seed recipe based on blockflower
	 * @param Flowerid the meta of BlockFlower
	 * @param Seed the seed to return
	 */
	public static void SeedRecipe(int Flowerid, Item Seed){
		GameRegistry.addRecipe(new ItemStack(Seed,2), "x", "x", 'x',new ItemStack(Blocks.red_flower, 1, Flowerid));
	}

}
