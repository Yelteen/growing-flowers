package com.gelberkaktus.growingflowers.GFextended;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;


public class BlockCropsMOD extends BlockCrops
{
	
    protected BlockCropsMOD()
    {
        super();
        setTickRandomly(true);
        float f = 0.5F;
        setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
        this.setHardness(0.0F);
        this.setStepSound(Block.soundTypeGrass);
        this.disableStats();
        this.setCreativeTab(CreativeTabs.tabMisc);
        
    }

    /**
     * Gets passed in the blockID of the block below and supposed to return true if its allowed to grow on the type of
     * blockID passed in. Args: blockID
     */
    protected boolean canThisPlantGrowOnThisBlockID(Block par1)
    {
        return par1 == Blocks.farmland || par1 == Blocks.grass;
    }

    /**
     * Ticks the block if it's been scheduled
     */
    public void updateTick(World world, BlockPos pos, IBlockState state, Random random)
    {
        super.updateTick(world, pos, state, random);

        if (world.getLightFromNeighbors(pos.up()) >= 9)
        {
        	int i = ((Integer)state.getValue(AGE)).intValue();

            if (i < 7)
            {
                float f = getGrowthChance(world.getBlockState(pos).getBlock(), world, pos);

                if (random.nextInt((int)(25F / f) + 1) == 0)
                {
                    i++;
                    world.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(i+1)), 2);
                }
            }
        }
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random par1Random)
    {
        int a;
        a = par1Random.nextInt(4);
    	a = a+1;
    	return a;
    }
    
}
