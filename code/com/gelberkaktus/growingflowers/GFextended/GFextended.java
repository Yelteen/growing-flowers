package com.gelberkaktus.growingflowers.GFextended;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemSeeds;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

import com.gelberkaktus.growingflowers.CommonProxy;
import com.gelberkaktus.growingflowers.Data;
import com.gelberkaktus.growingflowers.Lib;
import com.gelberkaktus.growingflowers.GFplus.Bloomsalad;
import com.gelberkaktus.growingflowers.GFplus.GFplus;
import com.gelberkaktus.growingflowers.GFpure.GFpure;


@Mod(modid=GFextended.MODID, name="Growing Flowers Extended", version=Data.VERSION, dependencies=Data.Required)

public class GFextended {
	private boolean blmode = GFpure.blmode;
	
	public static final String MODID = "GFExtended";
	public static final String ThingMODID = "GFExtended";
	
	@SidedProxy(clientSide = Data.clientproxy, serverSide = Data.serverproxy)
	public static CommonProxy proxy;
	
	//block-var
	public static Block	wool, 		mais, 		salad;
	public static Item 	woolSeed, 	maisSeed, 	saladSeed,
						woolitem, 	corn, 		saladitem,
									cornbaked,	saladeat;
	
	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
		initializeFoodandStuff();
		initializePlants();
		initializeSeeds();
		if (GFpure.extendedenabled||blmode){
			registerBlocks();
			registerItems();
		}
	}
	
	private void registerItems() {
		// TODO Auto-generated method stub
		Lib.register(woolSeed);
		Lib.register(maisSeed);
		Lib.register(saladSeed);
		Lib.register(woolitem);
		Lib.register(corn);
		Lib.register(saladitem);
		Lib.register(cornbaked);
		Lib.register(saladeat);
	}

	private void initializePlants() {
		// TODO Auto-generated method stub
		wool = new BlockPlantCotton().setUnlocalizedName("wool");
		mais = new BlockPlantCorn().setUnlocalizedName("mais");
		salad = new BlockPlantLettuce().setUnlocalizedName("salad");
	}


	private void initializeSeeds() {
		// TODO Auto-generated method stub
		woolSeed = new ItemSeeds(GFextended.wool, Blocks.farmland).setUnlocalizedName("woolseed");
		maisSeed = new ItemSeeds(GFextended.mais, Blocks.farmland).setUnlocalizedName("maisseed");
		saladSeed = new ItemSeeds(GFextended.salad, Blocks.farmland).setUnlocalizedName("saladseed");
	}


	private void initializeFoodandStuff() {
		// TODO Auto-generated method stub
		corn = new ItemFood(2, 2.0F, false).setUnlocalizedName("corn").setCreativeTab(CreativeTabs.tabFood);
		cornbaked = new ItemFood(6, 2.5F, false).setUnlocalizedName("cornbaked").setCreativeTab(CreativeTabs.tabFood);
		saladitem = new ItemFood(1, 0.1F, false).setUnlocalizedName("saladItem").setCreativeTab(CreativeTabs.tabMisc);
		
		saladeat = new Bloomsalad(7, 1.1F).setUnlocalizedName("saladEat").setCreativeTab(CreativeTabs.tabFood);
		
		woolitem = new Item().setUnlocalizedName("woolItem").setCreativeTab(CreativeTabs.tabMisc);
	}

	private void registerBlocks() {
		// TODO Auto-generated method stub
		GameRegistry.registerBlock(mais, "mais");
		GameRegistry.registerBlock(salad, "salad");
		GameRegistry.registerBlock(wool, "wool");
	}	

	private void registerItemModels(){
		//TODO regi itemodels
		Lib.registerInventoryItem(wool, MODID);
		Lib.registerInventoryItem(mais, MODID);
		Lib.registerInventoryItem(salad, MODID);
		Lib.registerInventoryItem(woolSeed, MODID);
		Lib.registerInventoryItem(maisSeed, MODID);
		Lib.registerInventoryItem(saladSeed, MODID);
		Lib.registerInventoryItem(woolitem, MODID);
		Lib.registerInventoryItem(corn, MODID);
		Lib.registerInventoryItem(saladitem, MODID);
		Lib.registerInventoryItem(cornbaked, MODID);
		Lib.registerInventoryItem(saladeat, MODID);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		if(event.getSide()== Side.CLIENT){
			registerItemModels();
		}
	    //baumwolle zu wolle
		GameRegistry.addRecipe(new ItemStack(Blocks.wool,2), "XX", "XX", 'X', new ItemStack(GFextended.woolitem));
		//baumwollesamen
		GameRegistry.addRecipe(new ItemStack(GFextended.woolSeed), "X", "Y", "Z", 'X', new ItemStack(Blocks.wool), 'Y', new ItemStack(Items.string), 'Z', new ItemStack(Items.wheat_seeds));
		//maissamen
		GameRegistry.addRecipe(new ItemStack(GFextended.maisSeed),  "XY", "YX",  'X', new ItemStack(Items.wheat), 'Y', new ItemStack(GFplus.bluSeed));
		GameRegistry.addShapelessRecipe(new ItemStack(GFextended.maisSeed, 2), new ItemStack(GFextended.corn));
		//salatsamen
		GameRegistry.addShapelessRecipe(new ItemStack(GFextended.saladSeed),  new ItemStack(GFpure.bloom[1]), new ItemStack(GFplus.bluBloom), new ItemStack(Items.wheat_seeds));
		//salatzubereit
		ItemStack leaf = new ItemStack(GFextended.saladitem,1);
		GameRegistry.addShapelessRecipe(new ItemStack(GFextended.saladeat,1), leaf, leaf, leaf, new ItemStack(Items.bowl));
		//GameRegistry.addRecipe(recipe)
		
		//maisbraten
		GameRegistry.addSmelting(GFextended.corn, new ItemStack(GFextended.cornbaked), 0.1F);
		
	 }
	
	@EventHandler
    public void PostInit(FMLPostInitializationEvent event)
    {
		
        
    if (blmode){
    	System.out.println("Detected Bartender Mod; adding Recipe");
    	//BLexclusive
        Item cornsoup = new Bloomsalad(6, 5.5F).setUnlocalizedName("maissoup").setCreativeTab(CreativeTabs.tabFood);
        GameRegistry.registerItem(cornsoup, cornsoup.getUnlocalizedName());
            //maisgrutze
//            ItemStack[] bartendertest = new ItemStack[9];
//            for(int i = 0; i<9; i++){
//            bartendertest[i] = new ItemStack(GFextended.corn);
//            }
////            bartendertest[0] = new ItemStack(GFextended.corn);
//            bartendertest[4] = new ItemStack(GFextended.cornbaked);
////            bartendertest[7] = new ItemStack(GFextended.corn);
//            
//            at.tk911.bartender.api.CraftingHandler.addRecipe(new ItemStack(cornsoup, 1), new ItemStack(Items.bowl), bartendertest);

    }
    }
}
