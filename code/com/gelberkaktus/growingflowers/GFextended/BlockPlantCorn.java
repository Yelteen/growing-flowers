package com.gelberkaktus.growingflowers.GFextended;
import java.util.Random;

import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BlockPlantCorn extends BlockCropsMOD{

	protected BlockPlantCorn() {
		super();
		//creativeTab
		this.setCreativeTab((CreativeTabs)null);
        this.setHardness(0.0F);
        
	}

    
    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
    {
    	
        if (world.isRemote)
        {
        	return true;
        }
    	
    	ItemStack itemstack = player.inventory.getCurrentItem();
    	//prufen, ob bonemeal
        if(itemstack != null && itemstack.getItem() == Items.dye)
        {
        	if(itemstack.getItemDamage() == 15)
            {
        		int grown = (Integer) world.getBlockState(pos).getValue(AGE);
            	int grow;
        		grow = MathHelper.getRandomIntegerInRange(world.rand, grown+1, 7);        		
        		if (!world.isRemote)
                {
                    world.playAuxSFX(2005, pos, 0);
                }           	

                    world.setBlockState(pos, state.withProperty(AGE, grow),2);
                    if (!player.capabilities.isCreativeMode)
                    {   itemstack.stackSize--;}
//                    world.setBlockToAir(pos);
            		}   
        } 
        
        return true;
    }
    
    /**
     * Drops the block items with a specified chance of dropping the specified items
     */
    public void dropBlockAsItemWithChance(World world, BlockPos pos, IBlockState state, float chance, int fortune)
    {
    	super.dropBlockAsItemWithChance(world, pos, state, chance, 0);

        if (world.isRemote)
        {
            return;
        }

        int i = 3 + fortune;

        for (int j = 0; j < i; j++)
        {
            if (world.rand.nextInt(15) <= chance)
            {
                float f = 0.7F;
                float f1 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
                float f2 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
                float f3 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
                EntityItem entityitem = new EntityItem(world, (float)pos.getX() + f1, (float)pos.getY() + f2, (float)pos.getZ() + f3, new ItemStack(GFextended.maisSeed));
                entityitem.setDefaultPickupDelay();
                world.spawnEntityInWorld(entityitem);
            }
        }
    }

    /**
     * Generate a seed ItemStack for this crop.
     */
    protected Object getSeedItem()
    {
        return GFextended.maisSeed;
    }

    /**
     * Generate a crop produce ItemStack for this crop.
     */
    protected Object getCropItem()
    {
        return GFextended.corn;
    }
    
    //variabel richtig falsch
    private boolean wachstum = false;
//    private boolean werkzeug = false; 
   

    
    /**
     * Returns the ID of the items to drop on destruction.
     */
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {	
    	int meta = ((Integer)state.getValue(AGE)).intValue();
    
    	//prufen ob ausgewachsen         	
    	if (meta == 7)
        {
        	wachstum = true;            
        }


        
        if (wachstum == true)
        {        	     	
        	return GFextended.corn;
        }
        //samen ausspucken, immer    
        return GFextended.maisSeed;
        
    };
    
    /**
     * only called by clickMiddleMouseButton , and passed to inventory.setCurrentItem (along with isCreative)
     */
    public Object idPicked(World world, BlockPos pos)
    {
    	return GFextended.maisSeed;
    }
}
