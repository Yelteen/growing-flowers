package com.gelberkaktus.growingflowers;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public abstract class Growing_Flower extends BlockBush
{
	//methods, who must be overridden
	/**
	 * gets Icon for meta
	 * @param par1 side
	 * @param par2 metadata
	 */
//	public abstract IIcon getIcon(int par1, int par2);
//	/**
//	 * register Block Icons
//	 */
//	public abstract void registerBlockIcons(IIconRegister par1IconRegister);
	/**
     * Returns the ID of the items to drop on destruction.
     */
	public abstract void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player);
	

    
	private Item stem, bloom;
	protected Item seed;
	private int stufereset, stufebloomdrop;
	public static final PropertyInteger AGE = PropertyInteger.create("age", 0, 7);
   
	
	/**
	 * Creates a growing Flower; classes needed
	 * @param stem STem dropped when used with shears
	 * @param bloom Bloom dropped when used with shears
	 * @param stufebloomdrop this meta and more drops bloom
	 * @param stufereset reset meta to this, when drop flower 
	 * @param seed seed dropped randomly
	 */
	protected Growing_Flower(Item stem, Item bloom, int stufebloomdrop, int stufereset, Item seed)
    {
        super(Material.plants);
        setTickRandomly(true);
        
        this.setDefaultState(this.blockState.getBaseState().withProperty(AGE, Integer.valueOf(0)));
        float f = 0.5F;
        setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
        this.setHardness(0.0F);
        this.setStepSound(Block.soundTypeGrass);
        this.disableStats();
        this.setCreativeTab((CreativeTabs)null);
        this.stem = stem;
        this.bloom = bloom;
        this.stufereset = stufereset;
        this.stufebloomdrop = stufebloomdrop;
        this.seed = seed;        
    }
	
	public IBlockState getStateFromMeta(int meta) {
		return this.getDefaultState().withProperty(AGE, Integer.valueOf(meta));
	}

	public int getMetaFromState(IBlockState state) {
		return ((Integer) state.getValue(AGE)).intValue();
	}

	protected BlockState createBlockState() {
		return new BlockState(this, new IProperty[] { AGE });
	}	
	/**
	 * returns the itemdamage for drop
	 */
	public int damageDropped(IBlockState i) {
		return (Integer) i.getValue(AGE);
	}
	
	/**
     * only called by clickMiddleMouseButton , and passed to inventory.setCurrentItem (along with isCreative)
     */
	@SideOnly(Side.CLIENT)
	@Override	
	public abstract ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos);

	protected Item getSeed()
    {
		return this.seed;
    }
	
	/**
     * Drops the block items with a specified chance of dropping the specified items
     */
    public void dropBlockAsItemWithChance(World world, BlockPos pos, IBlockState state, float chance, int fortune)
    																	//int par5, float par6, int par7)
    {
        super.dropBlockAsItemWithChance(world, pos, state, chance, 0);

        if (world.isRemote)
        {
            return;
        }

        int i = 3 + fortune;

        for (int j = 0; j < i; j++)
        {
            if (world.rand.nextFloat() <= chance)
            {
                float f = 0.7F;
                float f1 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
                float f2 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
                float f3 = world.rand.nextFloat() * f + (1.0F - f) * 0.5F;
                EntityItem entityitem = new EntityItem(world, (float)pos.getX() + f1, (float)pos.getY() + f2, (float)pos.getZ() + f3, new ItemStack(seed));
                entityitem.setDefaultPickupDelay();
                world.spawnEntityInWorld(entityitem);
            }
        }
    }
	
	
	
	
	public static Random Rdm = new Random();
	//bonemeal/schere abbau blume grow
    /**
     * Called upon block activation (right click on the block.)
     */
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (world.isRemote)
        {
        	return true;
        }
    	
    	ItemStack itemstack = player.inventory.getCurrentItem();
    	//prufen, ob bonemeal
        if(itemstack != null && itemstack.getItem() == Items.dye)
        {
        	if(itemstack.getItemDamage() == 15)
            {
        		int grown = (Integer) world.getBlockState(pos).getValue(AGE);
        		if (grown<7){
        			int grow;
        			grow = MathHelper.getRandomIntegerInRange(world.rand, grown+1, 7);        		
        			if (!world.isRemote)
                	{
                    	world.playAuxSFX(2005, pos, 0);
                	}           	

                	world.setBlockState(pos, state.withProperty(AGE, grow),2);
                	if (!player.capabilities.isCreativeMode){   itemstack.stackSize--;}
                	world.notifyBlockOfStateChange(pos, Blocks.air);
        		}
            }   
        } 
                
        
        if(itemstack != null && itemstack.getItem() == Items.shears)
        {
        	System.out.println("shearung");
        	if (((Integer) state.getValue(AGE)) >= this.stufebloomdrop){    
        System.out.println("shearung"+bloom.getUnlocalizedName());  		
        		Lib.dropItemStack(new ItemStack(bloom, 1+(MathHelper.getRandomIntegerInRange(Rdm, 0, 1))), world, pos);
        		world.setBlockState(pos, state.withProperty(AGE, stufereset),2);} 
        	else{
        System.out.println("shearung"+stem.getUnlocalizedName());  
        		Lib.dropItemStack(new ItemStack(stem, 1), world, pos);
        		world.setBlockState(pos, state.withProperty(AGE, 0),2); 
        	}
        }
        return true;
    }
    

    /**
     * Gets passed in the blockID of the block below and supposed to return true if its allowed to grow on the type of
     * blockID passed in. Args: blockID
     */
    @Override
	protected boolean canPlaceBlockOn(Block par1)
    {
        return par1 == Blocks.farmland || par1 == Blocks.grass;
    }

    /**
     * Ticks the block if it's been scheduled
     */
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);

        if (world.getLightFromNeighbors(pos.up()) >= 9)
        {
            int i = ((Integer)state.getValue(AGE)).intValue();

            if (i < 7)
            {
                float f = getGrowthChance(this, world, pos);

                if (rand.nextInt((int)(25F / f) + 1) == 0)
                {
                    i++;
                    world.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(i)), 2);
                }
            }
        }
    }

 
        
    /**
     * Gets the growth rate for the crop. Setup to encourage rows by halving growth rate if there is diagonals, crops on
     * different sides that aren't opposing, and by adding growth for every crop next to this one (and for crop below
     * this one). Args: block, world, pos
     */
    protected static float getGrowthChance(Block blockIn, World world, BlockPos pos)
    {
        float f = 1.0F;
        BlockPos blockpos1 = pos.down();

        for (int i = -1; i <= 1; ++i)
        {
            for (int j = -1; j <= 1; ++j)
            {
                float f1 = 0.0F;
                IBlockState iblockstate = world.getBlockState(blockpos1.add(i, 0, j));

                if (iblockstate.getBlock().canSustainPlant(world, blockpos1.add(i, 0, j), net.minecraft.util.EnumFacing.UP, (net.minecraftforge.common.IPlantable)blockIn))
                {
                    f1 = 1.0F;

                    if (iblockstate.getBlock().isFertile(world, blockpos1.add(i, 0, j)))
                    {
                        f1 = 3.0F;
                    }
                }

                if (i != 0 || j != 0)
                {
                    f1 /= 4.0F;
                }

                f += f1;
            }
        }

        BlockPos blockpos2 = pos.north();
        BlockPos blockpos3 = pos.south();
        BlockPos blockpos4 = pos.west();
        BlockPos blockpos5 = pos.east();
        boolean flag = blockIn == world.getBlockState(blockpos4).getBlock() || blockIn == world.getBlockState(blockpos5).getBlock();
        boolean flag1 = blockIn == world.getBlockState(blockpos2).getBlock() || blockIn == world.getBlockState(blockpos3).getBlock();

        if (flag && flag1)
        {
            f /= 2.0F;
        }
        else
        {
            boolean flag2 = blockIn == world.getBlockState(blockpos4.north()).getBlock() || blockIn == world.getBlockState(blockpos5.north()).getBlock() || blockIn == world.getBlockState(blockpos5.south()).getBlock() || blockIn == world.getBlockState(blockpos4.south()).getBlock();

            if (flag2)
            {
                f /= 2.0F;
            }
        }

        return f;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random par1Random)
    {
        int a;
        a = par1Random.nextInt(4);
    	a = a+1;
    	return a;
    } 
    
}
