package com.gelberkaktus.growingflowers.GFpure;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoublePlant;
import net.minecraft.block.BlockDoublePlant.EnumPlantType;
import net.minecraft.block.BlockSlab.EnumBlockHalf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class Growing_Flower_Sunflower_Top extends Growing_DoubleFlower_Top {
	
//	private static Item stem = GFpure.stem;
//	private static Item bloom = GFpure.bloom[3];
//	private static int stufe = 10;
//	private static Item seed = GFpure.seed[3];
	
	protected Growing_Flower_Sunflower_Top() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
    {return true;}
		
	/**
     * Ticks the block if it's been scheduled
     */
	@Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);

        if (world.getBlockLightOpacity(pos.up()) >= 9)
        {
            int i = ((Integer)state.getValue(AGE)).intValue();
            System.out.println(String.valueOf(i)+">>"+String.valueOf(i+1)+"natural");
      		
            if (i < 7)
            {
                float f = getGrowthChance(this,world, pos);

                if (rand.nextInt((int)(25F / f) + 1) == 0)
                {
                    i++;
                    world.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(i)), 2);
                    if (i == 7){
                    	world.setBlockState(pos, Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.SUNFLOWER).withProperty(BlockDoublePlant.HALF, EnumBlockHalf.TOP), 2);
                    	world.setBlockState(pos.down(), Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.SUNFLOWER), 2);                	
                    }
                }
            }         
        }
    }
	
	@Override
	public void grow(World worldIn, BlockPos pos, IBlockState state)
    {
        int i = ((Integer)state.getValue(AGE)).intValue() + MathHelper.getRandomIntegerInRange(worldIn.rand, 2, 5);

        if (i > 14)
        {
            i = 14;
        }

        worldIn.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(i)), 2);
    }
	
//	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
//    {
//        if (world.isRemote)
//        {
//        	return true;
//        }
//    	
//    	ItemStack itemstack = player.inventory.getCurrentItem();
//    	//prufen, ob bonemeal
//        if(itemstack != null && itemstack.getItem() == Items.dye)
//        {
//        	if(itemstack.getItemDamAGE() == 15)
//            {
//        		int grown = (Integer) world.getBlockState(pos).getValue(AGE);
//            	int grow;
//        		grow = MathHelper.getRandomIntegerInRange(world.rand, grown+1, 14);        		
//        		if (!world.isRemote)
//                {
//                    world.playAuxSFX(2005, pos, 0);
//                }           	
//
//                world.setBlockState(pos, grow,3);
//        System.out.println(String.valueOf(grown)+">>"+String.valueOf(grow)+"bonemeal");
//                if (grow > 7){
//                	//world.setBlockState(pos, new BlockDoublePlant())
//                	if ((world.isAirBlock(pos.up())) && ((!world.provider.getHasNoSky()) || (par3 < 254)))
//					{
//                		world.setBlockState(pos.up(), this, grow, 3);
//					}
//                	
//                }
//                if (!player.capabilities.isCreativeMode){  itemstack.stackSize--;}
//                world.setBlockToAir(pos);
//            }   
//         } 
//                
//        
//        if(itemstack != null && itemstack.getItem() == Items.shears)
//        {
//        	if (world.getBlockMetadata(pos) >= 6){        	        	
//        	Growing_Flower.dropItemStack(new ItemStack(bloom, 1+(MathHelper.getRandomIntegerInRange(Rdm, 0, 1))), world, pos);
//            world.setBlockState(pos, stufe,2);} 
//        	else{
//        	Growing_Flower.dropItemStack(new ItemStack(stem, 1), world, pos);
//        	world.setBlockState(pos, 0,2); 
//        	}
//        }
//        return true;
//    }
	
	@Override
	protected boolean canPlaceBlockOn(Block par1)
    {
		return par1 == GFpure.sunflowergrow;
    }

}
