package com.gelberkaktus.growingflowers.GFpure;

import java.util.Random;

import net.minecraft.block.BlockDoublePlant;
import net.minecraft.block.BlockDoublePlant.EnumPlantType;
import net.minecraft.block.BlockSlab.EnumBlockHalf;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.gelberkaktus.growingflowers.Growing_Flower;
import com.gelberkaktus.growingflowers.Lib;

public class Growing_Flower_Rose extends Growing_Flower
{		
	private static Item stem = GFpure.redstem;
	private static Item bloom = GFpure.bloom[9];
	private static Item seed = GFpure.seed[13];
	
	//var fur blockactivated
    private static int stufe = 3;
	
	protected Growing_Flower_Rose()
    {
    	 super(stem, bloom, 6, stufe, seed);
    }
	
	@Override
	public void onBlockHarvested(World world, BlockPos pos, IBlockState state,EntityPlayer player)
	{
		int meta = ((Integer)state.getValue(AGE)).intValue();
		if (!player.capabilities.isCreativeMode){
			Item seed = GFpure.seed[13];
			//System.out.println(seed.getUnlocalizedName());
			ItemStack drop = new ItemStack(seed, MathHelper.getRandomIntegerInRange(Rdm, 1, 4));;
			Lib.dropItemStack(drop, world, pos);
			if(meta >= 7){
				ItemStack drop2 = new ItemStack(GFpure.rose, MathHelper.getRandomIntegerInRange(Rdm, 1, 3));
				Lib.dropItemStack(drop2, world, pos);
			}			
		}
		world.setBlockToAir(pos);
	}
    
	@SideOnly(Side.CLIENT)
	@Override	
	public ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos)
    {
    	return new ItemStack(GFpure.seed[13], 1, 0);
    }


    public static Random Rdm = new Random();  
    //bonemeal/schere abbau blume grow
    /**
     * Called upon block activation (right click on the block.)
     */
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
    {   	
    	
        if (world.isRemote)
        {
        	return true;
        }
    	
    	ItemStack itemstack = player.inventory.getCurrentItem();
    	//prufen, ob bonemeal
        if(itemstack != null && itemstack.getItem() == Items.dye)
        {
        	if(itemstack.getItemDamage() == 15)
            {
        		int grown = (Integer) world.getBlockState(pos).getValue(AGE);
            	int grow;
            	if (grown >= 7){
            		grow = MathHelper.getRandomIntegerInRange(world.rand, grown+1, 10); 
        		}else{
        			grow = MathHelper.getRandomIntegerInRange(world.rand, grown+1, 7);	
            	}
        		if (!world.isRemote)
                {
                    world.playAuxSFX(2005, pos, 0);
                }        	
        		world.setBlockState(pos, state.withProperty(AGE, grow),2);
                if (!player.capabilities.isCreativeMode){ itemstack.stackSize--;}
            
				if (grow == 10){
        			//spawn rose
					if ((world.isAirBlock(pos.up())) && ((!world.provider.getHasNoSky()) || (pos.getY() < 254)))
					{
						world.setBlockToAir(pos);
						world.setBlockState(pos.up(), Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.ROSE).withProperty(BlockDoublePlant.HALF, EnumBlockHalf.TOP), 2);
	                	world.setBlockState(pos, Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.ROSE), 2);                	
	                }
        		}
            } 
    	}   
        
        if(itemstack != null && itemstack.getItem() == Items.shears)
        {
        	if ((Integer) world.getBlockState(pos).getValue(AGE) >= 6){        	        	
        	Lib.dropItemStack(new ItemStack(bloom, 1+(Rdm.nextInt(1))), world, pos);
            world.setBlockState(pos, state.withProperty(AGE, stufe),2);} 
        	else{
        	Lib.dropItemStack(new ItemStack(stem, 1), world, pos);
        	world.setBlockState(pos, state.withProperty(AGE, 0),2); 
        	}
        }
        return true;
    }    
    

    @Override
	public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);

        if (world.getBlockLightOpacity(pos.up()) >= 9)
        {
            int i = ((Integer)state.getValue(AGE)).intValue();

            if (i < 10)
            {
                float f = getGrowthChance(this, world, pos);

                if (rand.nextInt((int)(25F / f) + 1) == 0)
                {
                    i++;
                    world.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(i)), 2);
                }
            }
        }
    }
}