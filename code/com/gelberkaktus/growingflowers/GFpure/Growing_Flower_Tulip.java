package com.gelberkaktus.growingflowers.GFpure;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.gelberkaktus.growingflowers.Growing_Flower;
import com.gelberkaktus.growingflowers.Lib;

public class Growing_Flower_Tulip extends Growing_Flower{

	private int color = 0;
	//private static Item seed;
	//private static Item bloom;
	/**
	 * 
	 * @param bloom the bloom used
	 * @param seed the seed used
	 * @param color 0=red 1=orange 2=white 3=pink
	 */
	protected Growing_Flower_Tulip(int color) {
		super(GFpure.stem, GFpure.bloom[4+color], 6, 4, GFpure.tulipseed[color]);
		
		this.color = color;
		//Growing_Flower_Tulip.seed =  GFpure.tulipseed[color];
	}
	
	@Override
	public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player)
	{
		int meta = ((Integer)state.getValue(AGE)).intValue();
		if (!player.capabilities.isCreativeMode){
			Item seed = GFpure.tulipseed[color];
			//System.out.println(seed.getUnlocalizedName());
			ItemStack drop = new ItemStack(seed, MathHelper.getRandomIntegerInRange(Rdm, 1, 4));;
			Lib.dropItemStack(drop, world, pos);
			if(meta >= 6){
				ItemStack drop2 = new ItemStack(Blocks.red_flower, MathHelper.getRandomIntegerInRange(Rdm, 1, 3), 4+color);
				Lib.dropItemStack(drop2, world, pos);
			}			
		}
		world.setBlockToAir(pos);
	}
	
	@SideOnly(Side.CLIENT)
	@Override	
	public ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos)
    {
    	return new ItemStack(GFpure.tulipseed[color], 1, 0);
    }

}
