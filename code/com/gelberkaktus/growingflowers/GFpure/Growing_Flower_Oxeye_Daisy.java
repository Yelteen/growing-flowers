package com.gelberkaktus.growingflowers.GFpure;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.gelberkaktus.growingflowers.Growing_Flower;
import com.gelberkaktus.growingflowers.Lib;

public class Growing_Flower_Oxeye_Daisy extends Growing_Flower {
	
	private static Item stem = GFpure.stem;;
	private static Item bloom = GFpure.bloom[8];
	private static int stufebloomdrop = 7;
	private static int stufereset = 3;
	private static Item seed = GFpure.seed[8];

	protected Growing_Flower_Oxeye_Daisy() {
		super(stem, bloom, stufebloomdrop, stufereset, seed);
	}
	
	@Override
	public void onBlockHarvested(World world, BlockPos pos, IBlockState state,EntityPlayer player)
	{
		int meta = ((Integer)state.getValue(AGE)).intValue();
		if (!player.capabilities.isCreativeMode){
			Item seed = GFpure.seed[8];
			//System.out.println(seed.getUnlocalizedName());
			ItemStack drop = new ItemStack(seed, MathHelper.getRandomIntegerInRange(Rdm, 1, 4));;
			Lib.dropItemStack(drop, world, pos);
			if(meta >= 7){
				ItemStack drop2 = new ItemStack(Blocks.red_flower, MathHelper.getRandomIntegerInRange(Rdm, 1, 3), 8);
				Lib.dropItemStack(drop2, world, pos);
			}			
		}
		world.setBlockToAir(pos);
	}
	
	@SideOnly(Side.CLIENT)
	@Override	
	public ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos)
    {
    	return new ItemStack(GFpure.seed[8], 1, 0);
    }

}
