package com.gelberkaktus.growingflowers.GFpure;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoublePlant;
import net.minecraft.block.BlockDoublePlant.EnumPlantType;
import net.minecraft.block.BlockSlab.EnumBlockHalf;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.gelberkaktus.growingflowers.Lib;

public class Growing_Flower_Paeonia extends Growing_DoubleFlower_Bottom {

	private static Item stem = GFpure.stem;
	private static Item bloom = null;
	private static int stufe = 10;
	public static Item seed = GFpure.seed[12];
	
	public Growing_Flower_Paeonia() {
		super(stem, bloom, 7, stufe, seed);
		// TODO Auto-generated constructor stub
	}	


	@Override
	public void onBlockHarvested(World world, BlockPos pos, IBlockState state,EntityPlayer player)
	{
		int meta = ((Integer)state.getValue(AGE14)).intValue();
		if (!player.capabilities.isCreativeMode){
			Item seed = GFpure.seed[12];
			//System.out.println(seed.getUnlocalizedName());
			ItemStack drop = new ItemStack(seed, MathHelper.getRandomIntegerInRange(Rdm, 1, 4));;
			Lib.dropItemStack(drop, world, pos);
			if(meta >= 14){
				ItemStack drop2 = new ItemStack(Blocks.double_plant, MathHelper.getRandomIntegerInRange(Rdm, 0, 2), 5);
				Lib.dropItemStack(drop2, world, pos);
			}			
		}
		world.setBlockToAir(pos);
		if ((meta > 7)&&(world.getBlockState(pos.up()).getBlock().equals(GFpure.paeoniagrow_top))){
			world.setBlockToAir(pos.up());
		}
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override	
	public ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos)
    {
    	return new ItemStack(GFpure.seed[12], 1, 0);
    }
	
	@Override
	public Item getSeed(){ return Growing_Flower_Paeonia.seed;}
	
	/**
     * Ticks the block if it's been scheduled
     */
	@Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        super.updateTick(world, pos, state, rand);

        if (world.getLightFromNeighbors(pos.up()) >= 9)
        {
            int i = ((Integer)state.getValue(AGE14)).intValue();
      System.out.println(String.valueOf(i)+">>"+String.valueOf(i+1)+"natural");
      		float f = getGrowthChance(this,world, pos);
      		
            if ((i < 14)){ 
            	if((rand.nextInt((int)(25F / f) + 1) == 0)){
            		i++;
            		world.setBlockState(pos, state.withProperty(AGE14, Integer.valueOf(i)), 2);
            		if (i > 7){
                    	//world.setBlockState(pos, new BlockDoublePlant())
            			if ((world.isAirBlock(pos.up())) && ((!world.provider.getHasNoSky()) || (pos.getY() < 254)))
            			{
            				world.setBlockState(pos.up(), GFpure.paeoniagrow_top.getDefaultState().withProperty(AGE14, i-7), 3);
            			}else{
            				if(world.getBlockState(pos).getBlock() == GFpure.paeoniagrow_top){
            					world.setBlockState(pos, state.withProperty(AGE14, i-7), 3);
            				}
            			}
            	}	}
            }else{
                	world.setBlockState(pos.up(), Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.PAEONIA).withProperty(BlockDoublePlant.HALF, EnumBlockHalf.TOP), 2);
                	world.setBlockState(pos, Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.PAEONIA), 2);                	
            }
            
        }
    }


	@Override
	public void grow(World worldIn, BlockPos pos, IBlockState state)
    {
        int i = ((Integer)state.getValue(AGE14)).intValue() + MathHelper.getRandomIntegerInRange(worldIn.rand, 2, 5);

        if (i > 14)
        {
            i = 14;
        }

        worldIn.setBlockState(pos, state.withProperty(AGE14, Integer.valueOf(i)), 2);
    }
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		
        if (world.isRemote)
        {
        	return true;
        }
    	
    	ItemStack itemstack = player.inventory.getCurrentItem();
    	//prufen, ob bonemeal
        if(itemstack != null && itemstack.getItem() == Items.dye)
        {
        	if(itemstack.getItemDamage() == 15)
            {
        		int grown = (Integer) world.getBlockState(pos).getValue(AGE14);
            	int grow;
        		grow = MathHelper.getRandomIntegerInRange(world.rand, grown+1, grown+6);        		
        		if (!world.isRemote)
                {
                    world.playAuxSFX(2005, pos, 0);
                }           	

                world.setBlockState(pos, state.withProperty(AGE14, grow),3);
                System.out.println(String.valueOf(grown)+">>"+String.valueOf(grow)+"bonemeal");
                if (grow < 14){
                	if (grow > 7){  
                		if ((world.isAirBlock(pos.up())) && ((!world.provider.getHasNoSky()) || (pos.getY() < 254)))
						{
                			world.setBlockState(pos.up(), GFpure.paeoniagrow_top.getDefaultState().withProperty(AGE14, grow-7), 3);
						}else{
							if(world.getBlockState(pos) == GFpure.paeoniagrow_top){
								world.setBlockState(pos.up(), state.withProperty(AGE14, grow-7),3);
							}
						}                	
                	}
            	
        		}else{
        			world.setBlockState(pos.up(), Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.PAEONIA).withProperty(BlockDoublePlant.HALF, EnumBlockHalf.TOP), 2);
                	world.setBlockState(pos, Blocks.double_plant.getDefaultState().withProperty(BlockDoublePlant.VARIANT, EnumPlantType.PAEONIA), 2);                	
                }
                if (!player.capabilities.isCreativeMode){  itemstack.stackSize--;}
            }   
         }         
        return true;
    }
	
	@Override
	protected boolean canPlaceBlockOn(Block par1)
    {
		return par1 == Blocks.farmland || par1 == Blocks.grass;
    }
	
	@Override
	public IBlockState getStateFromMeta(int meta) {
		return this.getDefaultState().withProperty(AGE14, Integer.valueOf(meta));
	}

	@Override
	public int getMetaFromState(IBlockState state) {
		return ((Integer) state.getValue(AGE14)).intValue();
	}

	@Override
	protected BlockState createBlockState() {
		return new BlockState(this, new IProperty[] { AGE14 });
	}	
	/**
	 * returns the itemdamage for drop
	 */
	@Override
	public int damageDropped(IBlockState i) {
		return (Integer) i.getValue(AGE14);
	}

}
