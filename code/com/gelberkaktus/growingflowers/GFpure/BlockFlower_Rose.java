package com.gelberkaktus.growingflowers.GFpure;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BlockFlower_Rose extends BlockBush{

	public BlockFlower_Rose() {
		super(Material.plants);
		//this.icon = icon;
		//creativeTab
		this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setHardness(0.0F);
        this.setStepSound(Block.soundTypeGrass);
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, BlockPos pos, Entity par5Entity) 
	 {
		if(MathHelper.getRandomIntegerInRange(new Random(), 1, 5)==3){
		 par5Entity.attackEntityFrom(DamageSource.generic, 1);
		}
	 }
}
