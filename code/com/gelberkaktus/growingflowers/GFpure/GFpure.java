package com.gelberkaktus.growingflowers.GFpure;

import java.io.File;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

import com.gelberkaktus.growingflowers.CommonProxy;
import com.gelberkaktus.growingflowers.Data;
import com.gelberkaktus.growingflowers.Lib;

@Mod(modid=GFpure.MODID, name="Growing Flowers Pure", version=Data.VERSION, guiFactory="com.gelberkaktus.growingflowers.Growing_Flowers_GuiFactory")
public class GFpure {
	
	public static boolean blmode = false;
	
	public static final String MODID = "GFPure";
	
	@Mod.Instance(GFpure.MODID)
	public static GFpure instance;
	
//	public static Configuration configFile;
	
public static Configuration configFile;
	
	public static boolean plusenabled = false;
	public static boolean extendedenabled = false;
	//einzelne pflanzen aktivieren/deaktivieren machen!
//	private static boolean haschanged = false;	
	
	public static void syncConfig(){
		if(!blmode){
			plusenabled = configFile.getBoolean("Growing Flowers Plus", Configuration.CATEGORY_GENERAL, false,/* Data.translate("gui.config.gfplus"));/*/"Activate Growing Flowers Plus (Glowflower, Bushflower & Poisonflower)");
			extendedenabled = configFile.getBoolean("Growing Flowers Extended", Configuration.CATEGORY_GENERAL, false, /*Data.translate("gui.config.gfextended"));/*/"Activate Growing Flowers Extended (Corn, Lettuce, Cotton) Requires Plus for full functionality!!");
//			haschanged = configFile.hasChanged();	
		
		
			if(configFile.hasChanged()){			
//				//wenn plus weg dann extended auch weg udn umgekehrt
//				if(!plusenabled){ extendedenabled = false;}
//				if(extendedenabled){ plusenabled = false;}			
				configFile.save();
			}
		}
            
	}
	
	@SubscribeEvent
	public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs){
		if(eventArgs.modID.equals(MODID)){
//			haschanged = true;
			syncConfig();
//			haschanged = false;
		}
	}
	

	@SidedProxy(clientSide = Data.clientproxy, serverSide =Data.serverproxy)
	public static CommonProxy proxy;

	//block-var
	public static Block rose;
	
	//blumen
	public static Block poppygrow, dandeliongrow, alliumgrow, sunflowergrow, sunflowergrow_top;
	public static Block[] tulip = new Block[4];
	public static Block oxeyegrow, orchidegrow, houstoniagrow, syringagrow, syringagrow_top, paeoniagrow, paeoniagrow_top, rosegrow; 
//items
	//st�mme
	public static Item	redstem, stem;
	/*mohn, l�wenzahn, sternlauch, sonnenblume, tulpen (ora, pink, rot, wei�)
	* margerite, rose
	*/	 
	public static Item[] bloom = new Item[10];
	/*(mohn, l�wenzahn,) sternlauch, sonnenblume, tulpen (ora, pink, rot, wei�)
	* margerite, orchidee, houstonia, flieder, pfingstrose, rose
	*/
	public static Item[] seed = new Item[14];
	
	public static Item[] tulipseed = new Item[4];
	
	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
		configFile = new Configuration(new File(event.getModConfigurationDirectory(), "growing_flowers.cfg"));
		syncConfig();
		
		initializeBloomandStem();
		initializePlants();
		initializeSeeds();
		registerBlocks();
		registerItems();
	}


	private void initializeBloomandStem() {
		bloom[1] = (new Item()).setUnlocalizedName("dandelionbloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[0] = (new Item()).setUnlocalizedName("poppybloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[2] = (new Item()).setUnlocalizedName("alliumbloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[3] = null;//(new Item()).setUnlocalizedName("sunflowerbloom").setTextureName("GFPure:sunflower").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[4] = (new Item()).setUnlocalizedName("tulip_redbloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[5] = (new Item()).setUnlocalizedName("tulip_orangebloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[6] = (new Item()).setUnlocalizedName("tulip_whitebloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[7] = (new Item()).setUnlocalizedName("tulip_pinkbloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[8] = (new Item()).setUnlocalizedName("daisybloom").setCreativeTab(CreativeTabs.tabMaterials);
		bloom[9] = (new Item()).setUnlocalizedName("rosebloom").setCreativeTab(CreativeTabs.tabMaterials);
				
		redstem = (new Item()).setUnlocalizedName("rosestem").setCreativeTab(CreativeTabs.tabMaterials);
		stem = (new Item()).setUnlocalizedName("stem").setCreativeTab(CreativeTabs.tabMaterials);
				
	}
	
	private String[] tulipunlocname = new String[]{"red", "orange", "white", "pink"};	

	private void initializePlants() {
		rose = new BlockFlower_Rose().setUnlocalizedName("rose_little");
        		
		poppygrow = new Growing_Flower_Poppy().setUnlocalizedName("poppygrow");
		dandeliongrow =(new Growing_Flower_Dandelion()).setUnlocalizedName("dandeliongrow");
		alliumgrow = new Growing_Flower_allium().setUnlocalizedName("alliumgrow");
		sunflowergrow = new Growing_Flower_Sunflower().setUnlocalizedName("sunflowergrow");
			sunflowergrow_top = new Growing_Flower_Sunflower_Top().setUnlocalizedName("sunflowergrow_top");
		for(int i=0; i<4; i++){
			tulip[i] = new Growing_Flower_Tulip(i).setUnlocalizedName("tulip_grow_"+tulipunlocname[i]);
		}
		oxeyegrow = new Growing_Flower_Oxeye_Daisy().setUnlocalizedName("oxeyegrow");
		orchidegrow = new Growing_Flower_Orchidee().setUnlocalizedName("orchideegrow");
		houstoniagrow = new Growing_Flower_Houstonia().setUnlocalizedName("houstoniagrow");
		syringagrow = new Growing_Flower_Syringa().setUnlocalizedName("syringagrow");
			syringagrow_top = new Growing_Flower_Syringa_Top().setUnlocalizedName("syringagrow_top");
		paeoniagrow = new Growing_Flower_Paeonia().setUnlocalizedName("paeoniagrow");
			paeoniagrow_top = new Growing_Flower_Paeonia_Top().setUnlocalizedName("paeoniagrow_top");
	
		//paeoniagrow
		rosegrow =(new Growing_Flower_Rose()).setUnlocalizedName("rosegrow");
	}	

	public static void registerBlocks(){
		Lib.register(rosegrow);
		Lib.register(dandeliongrow);
		Lib.register(poppygrow);
		Lib.register(alliumgrow);
		Lib.register(oxeyegrow);
		Lib.register(orchidegrow);
		Lib.register(houstoniagrow);
		
		Lib.register(sunflowergrow);Lib.register(sunflowergrow_top);
		Lib.register(syringagrow);Lib.register(syringagrow_top);
		Lib.register(paeoniagrow);Lib.register(paeoniagrow_top);
		for(int i=0; i<4; i++){
			Lib.register(tulip[i]);
		}
		Lib.register(rose);
	}

	private void initializeSeeds() {
		seed[0]  = Data.addSeed(poppygrow, "poppy_seed");
		seed[1]  = Data.addSeed(dandeliongrow, "dandelion_seed");
		seed[2]  = Data.addSeed(alliumgrow, "allium_seed");
		seed[3] = Data.addSeed(sunflowergrow, "sunflower_seed");
		for(int i=0; i<4; i++){
			//getrocknet
			seed[4+i] = Data.addSeed(tulip[i], "seed_tulip_dried_"+tulipunlocname[i]);
			//ungetrockent
			tulipseed[i] = new Item().setUnlocalizedName("seed_tulip_raw_"+tulipunlocname[i]).setCreativeTab(CreativeTabs.tabMisc);
		}
		seed[8]  = Data.addSeed(oxeyegrow, "oxeye_seed");
		seed[9]  = Data.addSeed(orchidegrow, "orchidee_seed");
		seed[10] = Data.addSeed(houstoniagrow, "houstonia_seed");
		seed[11] = Data.addSeed(syringagrow, "syringa_seed");
		seed[12] = Data.addSeed(paeoniagrow, "paeonia_seed");
		seed[13] = Data.addSeed(rosegrow, "rose_seed");
		
	}
	
	private void registerItems(){
		Lib.register(redstem);
		Lib.register(stem);
		
		for (int i= 0; i<GFpure.bloom.length; i++){
			if (i!= 3){
			Lib.register(bloom[i]);}
		}

		for (int i= 0; i<GFpure.seed.length; i++){
			Lib.register(seed[i]);
		}
		for(int i=0; i<4; i++){
			Lib.register(tulipseed[i]);
		}

	}
	

	private void registerItemModels(){
		// regi itemodels
		Lib.registerInventoryItem(rose, MODID);
		
		//blumen
//		Lib.registerInventoryItem(poppygrow, MODID);
//		Lib.registerInventoryItem(dandeliongrow, MODID);
//		Lib.registerInventoryItem(alliumgrow, MODID);
//		Lib.registerInventoryItem(sunflowergrow, MODID);
//		Lib.registerInventoryItem(sunflowergrow_top, MODID);
//		for(int i = 0; i< tulip.length; i++){
//			Lib.registerInventoryItem(tulip[i], MODID);
//		}
//		Lib.registerInventoryItem(oxeyegrow, MODID);
//		Lib.registerInventoryItem(orchidegrow, MODID);
//		Lib.registerInventoryItem(houstoniagrow, MODID);
//		Lib.registerInventoryItem(syringagrow, MODID);
//		Lib.registerInventoryItem(syringagrow_top, MODID);
//		Lib.registerInventoryItem(paeoniagrow, MODID);
//		Lib.registerInventoryItem(paeoniagrow_top, MODID);
//		Lib.registerInventoryItem(rosegrow, MODID); 

		Lib.registerInventoryItem(redstem, MODID); 
		Lib.registerInventoryItem(stem, MODID); 	
		for(int i = 0; i< bloom.length; i++){
			if(bloom[i]!=null){
				Lib.registerInventoryItem(bloom[i], MODID);
			}			
		}
		for(int i = 0; i< seed.length; i++){
			Lib.registerInventoryItem(seed[i], MODID);
		}
		for(int i = 0; i< tulipseed.length; i++){
			Lib.registerInventoryItem(tulipseed[i], MODID);
		}
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		if(event.getSide()== Side.CLIENT){
			registerItemModels();
		}
		//config change?
				FMLCommonHandler.instance().bus().register(instance);	
		
		 //rezepte
		 Data.SeedRecipe(0, seed[0]);
		 Data.SeedRecipe(Blocks.yellow_flower, seed[1]);
		 Data.SeedRecipe(2, seed[2]);
		//sunflower
		 GameRegistry.addRecipe(new ItemStack(GFpure.seed[3], 2), "x", "x", 'x',new ItemStack(Blocks.double_plant, 1, 0));
		 
		 for(int i=4; i<8; i++){ //tulpen
			 Data.SeedRecipe(i, tulipseed[i-4]);}
		 Data.SeedRecipe(8, seed[8]);
		 Data.SeedRecipe(1, seed[9]);
		 Data.SeedRecipe(3, seed[10]);
		 //flieder
		 GameRegistry.addRecipe(new ItemStack(GFpure.seed[11], 2), "x", "x", 'x',new ItemStack(Blocks.double_plant, 1, 1));
		 //paeonia
		 GameRegistry.addRecipe(new ItemStack(GFpure.seed[12], 2), "x", "x", 'x',new ItemStack(Blocks.double_plant, 1, 5));
		 
		 Data.SeedRecipe(rose, seed[13]); 
		 
		 for(int i=0; i<4; i++){
			 GameRegistry.addSmelting(tulipseed[i], new ItemStack(seed[4+i]), 0.2F);
		 }
		//zu stangel
			GameRegistry.addRecipe(new ItemStack(redstem, 1), "X", "E", 'X', new ItemStack(GFpure.seed[13]), 'E', new ItemStack(Blocks.dirt));
			for(int i=0; i<3; i++){
			GameRegistry.addRecipe(new ItemStack(stem, 1), "X", "E", 'X',  new ItemStack(GFpure.seed[i]), 'E', new ItemStack(Blocks.dirt));
			}
			GameRegistry.addRecipe(new ItemStack(stem, 1), "X", "E", 'X',  new ItemStack(GFpure.seed[8]), 'E', new ItemStack(Blocks.dirt));
						
		//blute+stangel
			//l�wenzahn
			GameRegistry.addRecipe(new ItemStack(Blocks.yellow_flower), "B", "S", 'B', new ItemStack(GFpure.bloom[1]), 'S', new ItemStack(GFpure.stem));
			//mohn
			GameRegistry.addRecipe(new ItemStack(Blocks.red_flower), "B", "S", 'B', new ItemStack(GFpure.bloom[0]), 'S', new ItemStack(GFpure.stem));	
			//allium
			GameRegistry.addRecipe(new ItemStack(Blocks.red_flower, 1, 2), "B", "S", 'B', new ItemStack(GFpure.bloom[2]), 'S', new ItemStack(GFpure.stem));	
			//tulpen
			for(int i=4; i<8; i++){
				GameRegistry.addRecipe(new ItemStack(Blocks.red_flower, 1, i), "B", "S", 'B', new ItemStack(GFpure.bloom[i]), 'S', new ItemStack(GFpure.stem));	
			}
			//margerite
			GameRegistry.addRecipe(new ItemStack(Blocks.red_flower, 1, 8), "B", "S", 'B', new ItemStack(GFpure.bloom[8]), 'S', new ItemStack(GFpure.stem));	
			//rose
			GameRegistry.addRecipe(new ItemStack(GFpure.rose), "B", "S", 'B', new ItemStack(GFpure.bloom[9]), 'S', new ItemStack(GFpure.redstem));	
				
	}	
}
