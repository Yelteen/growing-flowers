package com.gelberkaktus.growingflowers.GFplus;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class BlockFlowerPoison extends BlockBush{

	protected BlockFlowerPoison() {
		super(Material.plants);
		//licht
		this.setLightLevel(0.1F);
		//creativeTab
		this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setHardness(0.0F);
        this.setStepSound(Block.soundTypeGrass);
	}
	
	 public void onEntityCollidedWithBlock(World world, BlockPos pos, Entity par5Entity) 
	 {
		 par5Entity.attackEntityFrom(DamageSource.generic, 2);
	 }
}
