package com.gelberkaktus.growingflowers.GFplus;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockFlowerBush extends BlockBush{

	protected BlockFlowerBush() {
		super(Material.plants);
		// TODO Auto-generated constructor stub
		//creativeTab
		this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setHardness(0.0F);
        this.setStepSound(Block.soundTypeGrass);
        
	}
	
}
