package com.gelberkaktus.growingflowers.GFplus;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

import com.gelberkaktus.growingflowers.CommonProxy;
import com.gelberkaktus.growingflowers.Data;
import com.gelberkaktus.growingflowers.Lib;
import com.gelberkaktus.growingflowers.GFpure.GFpure;

@Mod(modid=GFplus.MODID, name="Growing Flowers Plus", version=Data.VERSION, dependencies="required-after:GFPure")//, guiFactory="com.gelberkaktus.growingflowers.Growing_Flowers_GuiFactory")

public class GFplus{
	
	private boolean blmode = GFpure.blmode;
	
	public static final String MODID = "GFPlus";
	
	@Mod.Instance(GFplus.MODID)
	public static GFplus instance;
	
//	public static Configuration configFile;
//	
//	public static boolean plusenabled = false;
//	
//	public static void syncConfig(){
//		plusenabled = configFile.getBoolean("Growing Flowers Plus", Configuration.CATEGORY_GENERAL, false, "Growing Flowers is enabled");
//	
//		if(configFile.hasChanged()){
//			//wenn plus weg dann extended auch weg
//			if(!plusenabled){}
//			configFile.save();
//		}
//            
//	}
//	
//	@SubscribeEvent
//	public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs){
//		if(eventArgs.modID.equals(MODID)){
//			syncConfig();
//		}
//	}
	
	
	@SidedProxy(clientSide = Data.clientproxy, serverSide =Data.serverproxy)
	public static CommonProxy proxy;

	//block-var
	public static Block oraGrow, ora, 
						greGrow, gre, 
						bluGrow, blu;
	
	//item-var
	public static Item 	oraBloom, oraSeed, 
						greBloom, greSeed,
						bluBloom, bluSeed,
						
						bloomsalad;

	
	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
//		configFile = new Configuration(new File(event.getModConfigurationDirectory(), "growing_flowers.cfg"));
//		syncConfig();
		
		initializeBlooms();			
		initializePlants();
		initializeSeeds();
		if(GFpure.plusenabled||blmode){
			registerItems();
			registerBlocks();
		}
	}	

	private void registerItems() {
		Lib.register(oraBloom);
		Lib.register(oraSeed);
		Lib.register(greBloom);
		Lib.register(greSeed);
		Lib.register(bluBloom);
		Lib.register(bluSeed);
		Lib.register(bloomsalad);
	}
	
	private void registerBlocks() {
		Lib.register(blu);
		Lib.register(gre);
		Lib.register(ora);
		Lib.register(bluGrow);
		Lib.register(greGrow);
		Lib.register(oraGrow);
	}
	
	private void registerItemModels(){
		//TODO regi itemodels
		Lib.registerInventoryItem(oraGrow, MODID);				
		Lib.registerInventoryItem(ora, MODID);	
		Lib.registerInventoryItem(greGrow, MODID);
		Lib.registerInventoryItem(gre, MODID);
		Lib.registerInventoryItem(bluGrow, MODID);
		Lib.registerInventoryItem(blu, MODID);

//item-var
		Lib.registerInventoryItem(oraBloom, MODID);
		Lib.registerInventoryItem(oraSeed, MODID);
		Lib.registerInventoryItem(greBloom, MODID);
		Lib.registerInventoryItem(greSeed, MODID);
		Lib.registerInventoryItem(bluBloom, MODID);
		Lib.registerInventoryItem(bluSeed, MODID);	
		Lib.registerInventoryItem(bloomsalad, MODID);
	}

	private void initializeBlooms() {		
		oraBloom = new Item().setUnlocalizedName("glow_bloom").setCreativeTab(CreativeTabs.tabMaterials);
		greBloom = new Item().setUnlocalizedName("poisonous_bloom").setCreativeTab(CreativeTabs.tabMaterials);
		bluBloom = new ItemFood(3, 0.3F, false).setUnlocalizedName("bush_bloom").setCreativeTab(CreativeTabs.tabFood);
		bloomsalad = new Bloomsalad(8, 0.6F).setUnlocalizedName("bloomsalad").setCreativeTab(CreativeTabs.tabFood);
	}

	private void initializePlants() {
		oraGrow =(new Growing_Flower_Glowflower()).setUnlocalizedName("glowflower_grow");
		greGrow =(new Growing_Flower_Poisonflower()).setUnlocalizedName("poisionflower_grow");
		bluGrow =(new Growing_Flower_Bushflower()).setUnlocalizedName("bushflower_grow");
		//finished
		ora = (new BlockFlowerGlow()).setUnlocalizedName("glowflower");
		gre = (new BlockFlowerPoison()).setUnlocalizedName("poisionflower");
		blu = (new BlockFlowerBush()).setUnlocalizedName("bushflower").setStepSound(Block.soundTypeGrass);
	}


	private void initializeSeeds() {
		oraSeed = Data.addSeed(GFplus.oraGrow, "glow_seed");
		greSeed = Data.addSeed(GFplus.greGrow, "poisonous_seed");
		bluSeed = Data.addSeed(GFplus.bluGrow, "bush_seed");
	}
	
	public static void SeedRecipe(Block Flower, Item Seed){
		GameRegistry.addShapelessRecipe(new ItemStack(Seed,2), new ItemStack(Flower) ,new ItemStack(Flower));
	}
	
	@EventHandler
	 public void init(FMLInitializationEvent event) {
		
		if(event.getSide()== Side.CLIENT){
			registerItemModels();
		}
		
//		//config change?
//		FMLCommonHandler.instance().bus().register(instance);
		
		 //rezepte samen-Blume
		ItemStack oraseedstack = new ItemStack(oraSeed, 2);
		ItemStack oraflowerstack = new ItemStack(ora,1,0);
		ItemStack greseedstack = new ItemStack(greSeed, 2);
		ItemStack greflowerstack = new ItemStack(gre,1,0);
		ItemStack bluseedstack = new ItemStack(bluSeed, 2);
		ItemStack bluflowerstack = new ItemStack(blu,1,0);
		 GameRegistry.addShapelessRecipe(oraseedstack, oraflowerstack, oraflowerstack);
		 GameRegistry.addShapelessRecipe(greseedstack, greflowerstack, greflowerstack);
		 GameRegistry.addShapelessRecipe(bluseedstack, bluflowerstack, bluflowerstack);
		//Colors
		GameRegistry.addShapelessRecipe(new ItemStack(Items.dye, 1, 14), new Object[]{new ItemStack(GFplus.ora, 1)});
		GameRegistry.addShapelessRecipe(new ItemStack(Items.dye, 1, 10), new Object[]{new ItemStack(GFplus.gre, 1)});
		GameRegistry.addShapelessRecipe(new ItemStack(Items.dye, 1, 6), new Object[]{new ItemStack(GFplus.blu, 1)});
		//rezept blumesalat
		GameRegistry.addShapelessRecipe(new ItemStack(GFplus.bloomsalad), new ItemStack(Items.bowl) ,new ItemStack(GFplus.bluBloom,2), new ItemStack(GFplus.oraBloom), new ItemStack(GFplus.greBloom), new ItemStack(GFpure.bloom[0]), new ItemStack(GFpure.bloom[1]));
		
		//rezeptebaum
		//rezeptbaum - blumencraft
		//zu stangel
		GameRegistry.addRecipe(new ItemStack(GFpure.stem, 1), "X", "E", 'X',new ItemStack( oraSeed), 'E', new ItemStack(Blocks.dirt));
		GameRegistry.addRecipe(new ItemStack(GFpure.stem, 1), "X", "E", 'X', new ItemStack(greSeed), 'E', new ItemStack(Blocks.dirt));
		GameRegistry.addRecipe(new ItemStack(GFpure.stem, 1), "X", "E", 'X', new ItemStack(bluSeed), 'E', new ItemStack(Blocks.dirt));
		
		//carfting orange blume
		GameRegistry.addRecipe(new ItemStack(ora, 1), "A", "B", "C", 'A', new ItemStack(Items.glowstone_dust,1), 'B', new ItemStack(GFpure.bloom[1],1), 'C', new ItemStack(GFpure.stem,1));
		//carfting grune blume
		GameRegistry.addRecipe(new ItemStack(gre, 1), "A", "B", "C", 'A', new ItemStack(Items.spider_eye,1), 'B', new ItemStack(GFpure.bloom[1],1), 'C', new ItemStack(GFpure.redstem,1));
		GameRegistry.addRecipe(new ItemStack(gre, 1), "A", "B", "C", 'A', new ItemStack(Items.spider_eye,1), 'B', new ItemStack(GFpure.bloom[0],1), 'C', new ItemStack(GFpure.stem,1));
		//crafting blaue Blume
		GameRegistry.addRecipe(new ItemStack(blu, 1), "A", "B", "C", 'A', new ItemStack(GFpure.bloom[1],1), 'B', new ItemStack(GFpure.stem,1), 'C',new ItemStack(Items.potato,1));

		//giftkartoffel
		GameRegistry.addShapelessRecipe(new ItemStack(Items.poisonous_potato), new ItemStack(GFplus.greBloom),new ItemStack(Items.potato));
		
		
		//stangel+blute
		GameRegistry.addRecipe(new ItemStack(GFplus.ora), "B", "S", 'B', new ItemStack(GFplus.oraBloom), 'S', new ItemStack(GFpure.stem));
		GameRegistry.addRecipe(new ItemStack(GFplus.gre), "B", "S", 'B', new ItemStack(GFplus.greBloom), 'S', new ItemStack(GFpure.redstem));
		GameRegistry.addRecipe(new ItemStack(GFplus.blu), "B", "S", 'B', new ItemStack(GFplus.bluBloom), 'S', new ItemStack(GFpure.stem));
	 }	  
	
}
