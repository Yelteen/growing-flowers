package com.gelberkaktus.growingflowers.GFplus;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockFlowerGlow extends BlockBush{

	protected BlockFlowerGlow() {
		super(Material.plants);
		//licht
		this.setLightLevel(0.9F);
		//creativeTab
		this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setHardness(0.0F);
        this.setStepSound(Block.soundTypeGrass);
	}
	
}
