package com.gelberkaktus.growingflowers.GFplus;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class Bloomsalad extends ItemFood{
	
	public Bloomsalad(int heal, float hunger) {
		super(heal, hunger, false);
		this.maxStackSize= 1;
	}
	
    public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        super.onFoodEaten(par1ItemStack, par2World, par3EntityPlayer);
        return new ItemStack(Items.bowl);
    }

}
