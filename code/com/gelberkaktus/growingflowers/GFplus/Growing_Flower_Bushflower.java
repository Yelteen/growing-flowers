package com.gelberkaktus.growingflowers.GFplus;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.gelberkaktus.growingflowers.Growing_Flower;
import com.gelberkaktus.growingflowers.Lib;
import com.gelberkaktus.growingflowers.GFpure.GFpure;


public class Growing_Flower_Bushflower extends Growing_Flower
{
	//var fur blockactivated
	private static int stufe = 1;
	private static Item seed = GFplus.bluSeed;
	
    protected Growing_Flower_Bushflower()
    {
        super(GFpure.stem, GFplus.bluBloom, 6, stufe, seed);
    }
    
    @Override
   	public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer player)
   	{
    	int meta = ((Integer)state.getValue(AGE)).intValue();
   		if (!player.capabilities.isCreativeMode){
   			Item seed = GFplus.bluSeed;
   			ItemStack drop = new ItemStack(seed, MathHelper.getRandomIntegerInRange(Rdm, 1, 4));;
   			Lib.dropItemStack(drop, world, pos);
   			if(meta >= 6){
   				ItemStack drop2 = new ItemStack(GFplus.blu, MathHelper.getRandomIntegerInRange(Rdm, 1, 3));
   				Lib.dropItemStack(drop2, world, pos);
   			}			
   		}
   		world.setBlockToAir(pos);
   	}  
   
  	@SideOnly(Side.CLIENT)
	@Override	
	public ItemStack getPickBlock(MovingObjectPosition target, World world, BlockPos pos)
    {
    	return new ItemStack(GFplus.bluSeed, 1, 0);
    }


  	
}
