package com.gelberkaktus.growingflowers;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;

import com.gelberkaktus.growingflowers.GFpure.GFpure;

public class Growing_Flowers_GuiConfig extends GuiConfig {
	
	
	public Growing_Flowers_GuiConfig(GuiScreen parent){
		super(parent, new ConfigElement(GFpure.configFile.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(),
				GFpure.MODID, true, true, GuiConfig.getAbridgedConfigPath(GFpure.configFile.toString()), "Enable more flowers and stuff!");
	}
	
//	/**
//     * Called when the mouse is clicked.
//     */
//    @Override
//    protected void mouseClicked(int x, int y, int mouseEvent)
//    {
//        if (mouseEvent != 0 || !this.entryList.func_148179_a(x, y, mouseEvent))
//        {
//        	System.out.println(this.entryList.toString());
////            this.entryList.mouseClicked(x, y, mouseEvent);
//        	if(this.entryList.getListEntry(0).isChanged()){
//        		if((this.entryList.getListEntry(0).enabled())&&(!this.entryList.getListEntry(1).isDefault())){
////        			this.mouseClicked(this.entryList.getListEntry(1).getEntryRightBound(), y, mouseEvent);
//        		}
//        	}
//     
//            super.mouseClicked(x, y, mouseEvent);
//        }
//    }

}
