Growing Flowers 1.7 by gelber_kaktus 
requires Minecraft Forge version 1.7.10-10.13.0 (Minecraft 1.7.10)
-----------------------------------------------------------------


1.Installation
--------------
1. download and start Minecraft Forge Installer 
2. download Growing Flowers herunterladen (if necessary) and copy it to folder Mods in the minecraft dir
3. select as version your downloaded forge in the Minecraft launcher
4. Close all opened windows and enjoy

1. Enable more Features
-----------------------
At the main menu goto Mods, select Growing Flowers, press Config and enable what you want!


3.What's new?
--------------
 1. craft flowers to seeds (--> Recipes)
 2. breed seeds on hoed land
 3. planted flowers/plants drops (0-3) seeds while harvesting
 4. fully grown flowers and plants drops themselves (1-4 pieces) and (1-3) seeds
 5. glowflowers, that glows at night
 6. poinsonflower which harms you and others
 7. bushflower, yummy
 8. corn, procudes corncobs
 9. lettuce, also eatable
10. cotton plant, produces cotton, can be crafted to wool 


4.Recipes
---------

#- a flower

# => 2 flower seeds
#

more at forum

5. Compatibility with other mods
---------------------------------
1. the growing flower is standalone and do not change anything at the minecarft flower
2. An API is coming sson

6. Forum topics
--------------------------
http://minecraft.curseforge.com/mc-mods/222715-growing-flowers
http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2155787-growing-flowers
http://minecraft.de/showthread.php?92038

7. Changelog
------------
1.7   - Update for MC 1.7.10, merged all varaints	[24.07.14]
1.6   - Update for MC 1.7.2 				[03.06.14]
	(contains little rose and allnew Vanilla-Flowers)
1.5.5 - Pre-Update to be compatible 			[17.05.14]		
1.5.4 - Update for MC 1.6.4 				[17.10.13]
1.5.3 - Update for MC 1.6.2 				[09.07.13]
1.5.2 - Update for MC 1.5.2 				[19.05.13]
1.5.1 - Update for MC 1.5.1				[25.03.13]
1.5   -	portes to forge					[25.02.13]
1.1.1d-	Update for MC 1.4.6				[22.12.12]
1.1.1c-	Update for MC 1.4.4				[16.11.12]
1.1.1b- Update for MC 1.4.2				[30.10.12]
1.1.1a- Update for MC 1.3.2				[11.10.12]
1.1.1 - German Translations Namen; Fix			[14.08.12]
1.1   - New Config files for IDs			[09.08.12]
1.0   - Release 					[15.07.12]
 
